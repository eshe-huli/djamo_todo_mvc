# DjamoTodoMVC
## Description
Djamo applying homework
## Installation

```bash
$ npm install
```

## Running the app
To run the app, you must rename .env.example to .env then enter your database credentials
```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Stay in touch

- Author - [Ben Ouattara](https://www.linkedin.com/in/ouattaraben)

