import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { TaskCommandEntity } from '../entities/task.command.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class TaskCommandRepository {
  constructor(
    @InjectRepository(TaskCommandEntity, 'command_db')
    private taskCommandEntityRepository: Repository<TaskCommandEntity>,
  ) {}

  async createTask(title: string, content?: string, is_done?: boolean) {
    return this.taskCommandEntityRepository.save({
      title: title,
      content: content,
      is_done: is_done,
    });
  }

  async updateTask(
    id: string,
    title: string,
    content?: string,
    is_done?: boolean,
  ) {
    return await this.taskCommandEntityRepository.update(
      { id: id },
      { title: title, content: content, is_done: is_done },
    );
  }

  async removeTask(id: string) {
    return await this.taskCommandEntityRepository.delete({
      id: id,
    });
  }
}
