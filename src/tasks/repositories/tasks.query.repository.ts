import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { TaskQueryEntity } from '../entities/task.query.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class TaskQueryRepository {
  constructor(
    @InjectRepository(TaskQueryEntity, 'query_db')
    private taskQueryEntityRepository: Repository<TaskQueryEntity>,
  ) {}

  async findAllTasks(): Promise<TaskQueryEntity[]> {
    return this.taskQueryEntityRepository.find();
  }

  async findOneTaskById(id: string): Promise<TaskQueryEntity> {
    return this.taskQueryEntityRepository.findOne({
      where: { id: id },
    });
  }
}
