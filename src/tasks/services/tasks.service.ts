import { Injectable } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { GetAllTasksQuery } from '../useCases/queries/getAllTasks.query';
import { GetTaskByIdQuery } from '../useCases/queries/getTaskById.query';
import { CreateTaskDto } from '../dtos/createTask.dto';
import { CreateTaskCommand } from '../useCases/commands/createTask.command';
import { UpdateTaskDto } from '../dtos/updateTask.dto';
import { UpdateTaskCommand } from '../useCases/commands/updateTask.command';
import { DeleteTaskCommand } from '../useCases/commands/deleteTask.command';

@Injectable()
export class TaskService {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  async getAllTasks() {
    return await this.queryBus.execute(new GetAllTasksQuery());
  }

  async getTaskById(id: string) {
    return await this.queryBus.execute(new GetTaskByIdQuery(id));
  }

  async addTask(createTaskDto: CreateTaskDto) {
    const title = createTaskDto.title;
    const content = createTaskDto.content;
    const is_done = createTaskDto.is_done;
    return await this.commandBus.execute(
      new CreateTaskCommand(title, content, is_done),
    );
  }

  async updateTask(id: string, updateTaskDto: UpdateTaskDto) {
    const title = updateTaskDto.title;
    const content = updateTaskDto.content;
    const is_done = updateTaskDto.is_done;
    return await this.commandBus.execute(
      new UpdateTaskCommand(id, title, content, is_done),
    );
  }

  async deleteTask(id: string) {
    return await this.commandBus.execute(new DeleteTaskCommand(id));
  }
}
