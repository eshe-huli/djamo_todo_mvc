export class CreateTaskCommand {
  constructor(
    public readonly title: string,
    public readonly content: string,
    public readonly is_done?: boolean,
  ) {}
}
