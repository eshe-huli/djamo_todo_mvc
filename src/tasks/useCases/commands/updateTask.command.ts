export class UpdateTaskCommand {
  constructor(
    public readonly id: string,
    public readonly title?: string,
    public readonly content?: string,
    public readonly is_done?: boolean,
  ) {}
}
