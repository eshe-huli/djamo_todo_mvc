import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { DeleteTaskCommand } from '../deleteTask.command';
import { TaskCommandRepository } from '../../../repositories/tasks.command.repository';
import { NotFoundException } from '@nestjs/common';

@CommandHandler(DeleteTaskCommand)
export class DeleteTaskHandler implements ICommandHandler<DeleteTaskCommand> {
  constructor(
    private readonly taskCommandEntityRepository: TaskCommandRepository,
  ) {}

  async execute(deleteTaskCommand: DeleteTaskCommand) {
    const deletedTask = await this.taskCommandEntityRepository.removeTask(
      deleteTaskCommand.id,
    );

    if (deletedTask.affected === 0) {
      throw new NotFoundException();
    }
    return true;
  }
}
