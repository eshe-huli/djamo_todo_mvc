import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { CreateTaskCommand } from '../createTask.command';
import { TaskCommandRepository } from '../../../repositories/tasks.command.repository';

@CommandHandler(CreateTaskCommand)
export class CreateTaskHandler implements ICommandHandler<CreateTaskCommand> {
  constructor(
    private readonly taskCommandEntityRepository: TaskCommandRepository,
  ) {}

  async execute(createTaskCommand: CreateTaskCommand) {
    return this.taskCommandEntityRepository.createTask(
      createTaskCommand.title,
      createTaskCommand.content,
      createTaskCommand.is_done,
    );
  }
}
