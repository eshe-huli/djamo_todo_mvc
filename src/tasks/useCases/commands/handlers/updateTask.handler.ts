import { ICommandHandler, CommandHandler } from '@nestjs/cqrs';
import { UpdateTaskCommand } from '../updateTask.command';
import { TaskCommandRepository } from '../../../repositories/tasks.command.repository';
import { NotFoundException } from '@nestjs/common';

@CommandHandler(UpdateTaskCommand)
export class UpdateTaskHandler implements ICommandHandler<UpdateTaskCommand> {
  constructor(
    private readonly taskCommandEntityRepository: TaskCommandRepository,
  ) {}

  async execute(updateTaskCommand: UpdateTaskCommand) {
    const updatedTask = await this.taskCommandEntityRepository.updateTask(
      updateTaskCommand.id,
      updateTaskCommand.title,
      updateTaskCommand.content,
      updateTaskCommand.is_done,
    );

    if (updatedTask.affected === 0) {
      throw new NotFoundException();
    }
    return true;
  }
}
