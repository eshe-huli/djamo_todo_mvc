import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetTaskByIdQuery } from '../getTaskById.query';
import { TaskQueryRepository } from '../../../repositories/tasks.query.repository';
import { NotFoundException } from '@nestjs/common';

@QueryHandler(GetTaskByIdQuery)
export class GetTaskByIdHandler implements IQueryHandler<GetTaskByIdQuery> {
  constructor(private readonly taskQueryRepository: TaskQueryRepository) {}

  async execute(command: GetTaskByIdQuery) {
    const task = await this.taskQueryRepository.findOneTaskById(command.id);
    if (!task) {
      throw new NotFoundException();
    }
    return task;
  }
}
