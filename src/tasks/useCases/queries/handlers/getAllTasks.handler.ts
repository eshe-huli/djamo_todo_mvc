import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetAllTasksQuery } from '../getAllTasks.query';
import { TaskQueryRepository } from '../../../repositories/tasks.query.repository';

@QueryHandler(GetAllTasksQuery)
export class GetAllTasksHandler implements IQueryHandler<GetAllTasksQuery> {
  constructor(private readonly taskQueryRepository: TaskQueryRepository) {}

  async execute() {
    return await this.taskQueryRepository.findAllTasks();
  }
}
