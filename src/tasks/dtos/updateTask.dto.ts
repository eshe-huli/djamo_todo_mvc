import { IsBoolean, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class UpdateTaskDto {
  @IsString()
  @IsNotEmpty()
  @IsOptional()
  public title: string;

  @IsString()
  @IsOptional()
  public content: string;

  @IsBoolean()
  @IsOptional()
  public is_done?: boolean;
}
