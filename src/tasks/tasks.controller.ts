import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { TaskService } from './services/tasks.service';
import { CreateTaskDto } from './dtos/createTask.dto';
import { UpdateTaskDto } from './dtos/updateTask.dto';

@Controller('tasks')
export class TasksController {
  constructor(private readonly taskService: TaskService) {}

  @Get('')
  async index() {
    return await this.taskService.getAllTasks();
  }

  @Get(':id')
  async show(@Param() params) {
    const id = params.id;
    return await this.taskService.getTaskById(id);
  }

  @Post()
  async store(@Body() createTaskDto: CreateTaskDto) {
    return await this.taskService.addTask(createTaskDto);
  }

  @Patch(':id')
  async update(@Param() params, @Body() updateTaskDto: UpdateTaskDto) {
    const id = params.id;
    await this.taskService.updateTask(id, updateTaskDto);
    return await this.taskService.getTaskById(id);
  }

  @Delete(':id')
  async delete(@Param() params) {
    await this.taskService.deleteTask(params.id);
    return { message: 'Task Deleted' };
  }
}
