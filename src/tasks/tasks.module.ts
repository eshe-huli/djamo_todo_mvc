import { Module } from '@nestjs/common';
import { TasksController } from './tasks.controller';
import { TaskService } from './services/tasks.service';
import { CqrsModule } from '@nestjs/cqrs';
import { GetAllTasksHandler } from './useCases/queries/handlers/getAllTasks.handler';
import { Repository } from 'typeorm';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TaskQueryEntity } from './entities/task.query.entity';
import { GetTaskByIdHandler } from './useCases/queries/handlers/getTaskById.handler';
import { CreateTaskHandler } from './useCases/commands/handlers/createTask.handler';
import { TaskCommandEntity } from './entities/task.command.entity';
import { UpdateTaskHandler } from './useCases/commands/handlers/updateTask.handler';
import { DeleteTaskHandler } from './useCases/commands/handlers/deleteTask.handler';
import { TaskQueryRepository } from './repositories/tasks.query.repository';
import { TaskCommandRepository } from './repositories/tasks.command.repository';

@Module({
  imports: [
    CqrsModule,
    TypeOrmModule.forFeature([TaskQueryEntity], 'query_db'),
    TypeOrmModule.forFeature([TaskCommandEntity], 'command_db'),
  ],
  exports: [TaskQueryRepository, TaskCommandRepository],
  controllers: [TasksController],
  providers: [
    TaskService,
    GetAllTasksHandler,
    TaskQueryRepository,
    TaskCommandRepository,
    Repository,
    GetTaskByIdHandler,
    CreateTaskHandler,
    UpdateTaskHandler,
    DeleteTaskHandler,
  ],
})
export class TasksModule {}
