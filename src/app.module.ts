import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TasksModule } from './tasks/tasks.module';
import { TaskQueryEntity } from './tasks/entities/task.query.entity';
import { TaskCommandEntity } from './tasks/entities/task.command.entity';

@Module({
  imports: [
    TasksModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      name: 'query_db',
      type: 'postgres',
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      entities: ['./dist/**/*.query.entity{.ts,.js}'],
      synchronize: true,
      autoLoadEntities: true,
      logging: true,
    }),

    TypeOrmModule.forRoot({
      name: 'command_db',
      type: 'postgres',
      host: process.env.WRITE_DB_HOST,
      port: parseInt(process.env.WRITE_DB_PORT),
      username: process.env.WRITE_DB_USERNAME,
      password: process.env.WRITE_DB_PASSWORD,
      database: process.env.WRITE_DB_DATABASE,
      entities: ['./dist/**/*.command.entity{.ts,.js}'],
      synchronize: true,
      autoLoadEntities: true,
      logging: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
